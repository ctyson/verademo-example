stages:
  - build
  - security-scan
  - scan_reporting
  - commit_baseline

Compile Application:
  image: maven:3.6.0-jdk-8
  stage: build
  except:
    changes:
      - pipeline-base line.json
  script:
    - mvn clean package
  artifacts:
    name: verademo_build
    paths:
      - target/
    expire_in: 5 weeks
  cache:
    paths:
      - target;
      - .m2/repository

Software Composition Analysis App 3rd party:
    image: maven:3.6.0-jdk-8
    stage: security-scan
    except:
      changes:
        - pipeline-baseline.json
    before_script:
        - curl -sL https://deb.nodesource.com/setup_17.x | bash -
        - apt-get update && apt-get -y install nodejs
        - npm install axios
        - npm install mathjs
    script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan . --update-advisor --allow-dirty --json scaResults.json --scan-collectors maven 2>&1 | tee sca_output.txt
    after_script:
        - nodejs ./veracode-helper/dependencies_app.js ${PRIVATE_TOKEN} true 41826058
    artifacts:
        reports:
            dependency_scanning: output-sca-vulnerabilites.json
        paths:
            - sca_output.txt
            - output-sca-vulnerabilites.json
        when: always
        name: "veracode-SCA-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true

Pipeline Scan Static Analysis:
  image: veracode/pipeline-scan:latest
  stage: security-scan
  only:
    - Feature-123
  except:
    refs:
      - schedules
    changes:
      - pipeline-baseline.json
  script:
    - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --request_policy "Financial" 2>&1 | tee pipeline_scan_policy_output.txt
    - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file target/verademo.war --issue_details true --gl_issue_generation true --gl_vulnerability_generation true --policy_file Financial.json -fjf filtered_results.json 2>&1 | tee pipeline_scan_text_output.txt
  artifacts:
        reports: 
            sast: veracode_gitlab_vulnerabilities.json
        paths:
            - results.json
            - filtered_results.json
            - pipeline_scan_text_output.txt
            - pipeline_scan_policy_output.txt
            - veracode_gitlab_vulnerabilities.json
        when: always
        name: "veracode-pipeline-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
  allow_failure: true

Sandbox Scan Static Analysis:
  image: veracode/api-wrapper-java
  stage: security-scan
  only:
    - schedules
  script:
    - java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
      -action UploadAndScan -appname "Verademo-example" -createprofile true -autoscan true
      -sandboxname "gitlab-feature-branch" -createsandbox true
      -filepath ./target/verademo.war -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}"
      -scantimeout 15 2>&1 | tee policy_scan_output.txt
  artifacts:
    paths:
      - policy_scan_output.txt
    when: always
    name: "veracode-POLICY-SCAN-SCI_COMMIT_REF_NAME-SCI_COMMIT_SHORT_SHA" 
  allow_failure: true   

Policy Scan Static Analysis:
  image: veracode/api-wrapper-java
  stage: security-scan
  only:
    - merge_request
  script:
    - java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
      -action UploadAndScan -appname "Verademo-example" -createprofile true -autoscan true
      -filepath ./target/verademo.war -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}"
      -scantimeout 15 2>&1 | tee policy_scan_output.txt
  artifacts:
    paths:
      - policy_scan_output.txt
    when: always
    name: "veracode-POLICY-SCAN-SCI_COMMIT_REF_NAME-SCI_COMMIT_SHORT_SHA"
  allow_failure: true

Generate static analysis report and issues - Sandbox Scan:
  image: node:latest
  stage: scan_reporting
  only:
    - schedules
  before_script:
    - cd veracode-helper
    - npm ci
  script:
    - npm run results-import scan_type=sandbox profile_name=Verademo-example sandbox_name=gitlab-feature-branch gitlab_token=${PRIVATE_TOKEN} gitlab_project=41826058 create_issue=true
  artifacts:
      reports:
          sast: veracode-helper/output-sast-vulnerabilites.json
      paths: 
          - veracode-helper/output-sast-vulnerabilites.json
      when: always
      name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"

Generate static analysis report and issues - Policy Scan:
    image: node:latest
    stage: scan_reporting
    only:
        - merge_request
    except:
      changes:
        - pipeline-baseline.json
    before_script:
        - cd veracode-helper
        - npm ci
    script:
        - npm run results-import scan_type=policy profile_name=Verademo-example gitlab_token=${PRIVATE_TOKEN} gitlab_project=41826058 create_issue=true  
    artifacts:
        reports:
            sast: veracode-helper/output-sast-vulnerabilites.json
        paths: 
            - veracode-helper/output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"

Pipeline Scan create new baseline file:
  image: veracode/pipeline-scan:latest
  stage: scan_reporting
  only:
    - merge_request
  except:
    changes:
      - pipeline-baseline.json
  script:
    - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file target/verademo.war -jf pipeline-baseline.json 2>&1 | tee pipeline_baseline_file_output.txt
  artifacts:
      paths:
          - pipeline-baseline.json
          - pipeline_baseline_file_output.txt
      when: always
      name: "veracode-pipeline-baseline-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
  allow_failure: true

Pipeline Scan baseline file commit:
    stage: commit_baseline
    when: on_success
    only:
        - merge_request
    except:
      changes:
        - pipeline-baseline.json
    before_script:
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - ssh-keyscan -t rsa $CI_SERVER_HOST >> ~/.ssh/known_hosts
        - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
        - git config http.sslverify false
        - git config --global user.name "${GITLAB_USER_NAME}"
        - git config --global user.email "${GITLAB_USER_EMAIL}"
    script:
        - git checkout -b Feature-123
        - git add -f pipeline-baseline.json
        - git commit -m "Pipeline Baseline from $CI_COMMIT_SHORT_SHA" || echo "No changes, nothing to commit!"
        - git remote rm origin && git remote add origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git
        - git push origin HEAD:Feature-123
    allow_failure: true

